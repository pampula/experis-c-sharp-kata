﻿using System.Text;

StringBuilder sb = new StringBuilder();
const int innerSquareMinSize = 6;

while (true)
{
    Console.WriteLine("Input square size as a non-negative whole number:");
    bool waitingSize = true;

    while (waitingSize)
    {
        string input = Console.ReadLine();
        if(int.TryParse(input, out var number))
        {
            if(number < 1)
            {
                Console.Clear();
                Console.Error.WriteLine($"Input value needs to be 1 or higher. ({input})");
                waitingSize = false;
            }
            else
            {
                string inner = "";
                if (number >= innerSquareMinSize)
                {
                    Console.WriteLine("Do you want to draw inner square (y/n)?");
                    inner = Console.ReadLine();
                }
                
                DrawSquare(number, inner.ToUpper() == "Y");
                waitingSize = false;
            }
        }
        else
        {
            Console.Clear();
            Console.Error.WriteLine($"Input is not valid! ({input})");
            waitingSize = false;
        }
    }
}

void DrawSquare(int size, bool drawInner = true)
{
    sb.Clear();
    for (int currentRow = 0; currentRow < size; ++currentRow) // Rows
    {
        for (int currentColumn = 0; currentColumn < size; ++currentColumn) // Columns
        {
            if (currentRow == 0 || currentRow == size - 1) // Draw first and last row
            {
                sb.Append('*');
            }
            else if (drawInner && (currentRow == 2 || currentRow == size - 3)) // Top and bottom rows for inner square
            {
                if(currentColumn != 1 && currentColumn != size - 2) // Omit space between squares
                {
                    sb.Append('*');
                }
                else
                {
                    sb.Append(' ');
                }
            }
            else // Middle rows and inner square top and bottom
            {
                if((currentColumn == 0 || currentColumn == size - 1) || (drawInner && (currentColumn == 2 || currentColumn == size - 3) && (currentRow >= 3 && currentRow <= size - 3)))
                {
                    sb.Append('*');
                }
                else
                {
                    sb.Append(' ');
                }
            }

            if (currentColumn == size - 1) // Last column, add a linebreak
            {
                sb.Append(Environment.NewLine);
            }
        }
    }
    
    Console.WriteLine(sb.ToString());
}