using reducto_multiplicitum;

namespace reducto_multiplicitum_tests;

public class UnitTest1
{
    [Theory]
    [InlineData(0, 0)]
    [InlineData(9, 9)]
    [InlineData(7, 9, 8)]
    [InlineData(6, 16, 28)]
    [InlineData(1, 111111111)]
    [InlineData(2, 1, 2, 3, 4, 5, 6)]
    [InlineData(6, 8, 16, 89, 3)]
    [InlineData(6, 26, 497, 62, 841)]
    [InlineData(6, 17737, 98723, 2)]
    [InlineData(8, 123, -99)]
    [InlineData(8, 167, 167, 167, 167, 167, 3)]
    [InlineData(2, 98526, 54, 863, 156489, 45, 6156)]
    public void Test1(int expected, params int[] numbers)
    {
        // Arrange
        var sut = new Reducer();
        
        // Act
        var result = sut.SumDigitProd(String.Join(",", numbers));

        // Assert
        Assert.Equal(expected, result);
    }
}