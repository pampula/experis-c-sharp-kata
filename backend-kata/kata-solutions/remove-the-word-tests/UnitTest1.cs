using remove_the_word;

namespace remove_the_word_tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("stringw", "string", "w")]
        [InlineData("bbllgnoaw", "balloon", "bgw")]
        [InlineData("anryow", "norway", "")]
        [InlineData("ttestu", "testing", "tu")]
        [InlineData("etstus", "t", "estus")]
        [InlineData("etstus", "st", "etus")]
        [InlineData("#%/%", "%", "#/%")]
        public void RemoveLetters_WithDifferentCombinations_ShouldRemoveCorrectLetters(string letters, string removed, string expected)
        {
            // Arrange
            var sut = new LetterRemover();
            // Act
            var result = sut.RemoveLetters(letters, removed);
            // Assert
            Assert.Equal(result, expected);
        }
    }
}