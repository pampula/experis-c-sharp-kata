﻿namespace freddy_fridays
{
    public class FreddyFridays
    {
        public const int MinYear = 1970;
        public const int MaxYear = 3000;

        public bool CheckValidInput(string input, out int output)
        {
            if(int.TryParse(input, out int result))
            {
                output = result;
                return true;
            }
            else
            {
                throw new Exception(($"'{input}' is not a valid numerical input."));
            }
        }
        
        public int FindFreddyFridays(int selectedYear)
        {
            if (selectedYear < MinYear || selectedYear > MaxYear)
            {
                throw new Exception(($"'{selectedYear}' is not a valid year."));
            }
            
            var fridays = 0;
            for (int i = 1; i <= 12; ++i)
            {
                var date = new DateOnly(selectedYear, i, 13);
                if (date.DayOfWeek == DayOfWeek.Friday) fridays++;
            }

            return fridays;
        }

        public List<int> FindYearsWithAmountOfFreddyFridays(int fridayCount)
        {
            List<int> years = new List<int>();
            for (int i = MinYear; i <= MaxYear; ++i)
            {
                int fridays = FindFreddyFridays(i);
                if (fridays == fridayCount)
                {
                    years.Add(i);
                }
            }

            return years;
        }

#if FindYearsWithMostFreddyFridays
        public List<int> FindYearsWithMostFreddyFridays()
        {
            int maxFridays = 0;
            List<int> years = new List<int>();
            for (int i = MinYear; i <= MaxYear; ++i)
            {
                int fridays = FindFreddyFridays(i);
                if (fridays > maxFridays)
                {
                    years.Clear();
                    maxFridays = fridays;
                    years.Add(i);
                }
                else if (fridays == maxFridays)
                {
                    years.Add(i);
                }
            }

            return years;
        }
#endif
    }
}