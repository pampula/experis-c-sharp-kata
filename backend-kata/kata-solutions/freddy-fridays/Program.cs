﻿using freddy_fridays;

var fridayFinder = new FreddyFridays();
Console.WriteLine("Friday the 13th Finder");

while (true)
{
    Console.WriteLine($"Input a year between 1970 and 3000 or a single digit to show years with that amount of Friday the 13ths.");
    var inputString = Console.ReadLine();
    try
    {
        if (inputString != null && fridayFinder.CheckValidInput(inputString, out var input))
        {
            switch (input)
            {
                case >= 0 and <= 9:
                {
                    List<int> foundYears = fridayFinder.FindYearsWithAmountOfFreddyFridays(input);
                    if (foundYears.Count == 0)
                    {
                        Console.Write($"Did not find any years with {input} Friday the 13ths.");
                    }
                    else
                    {
                        foreach (var year in foundYears)
                        {
                            Console.Write($"{year} ");
                        }
                    }

                    Console.Write(Environment.NewLine);
                    break;
                }
                default:
                    Console.WriteLine(
                        $"Year {input} has {fridayFinder.FindFreddyFridays(input)} Friday the 13ths.");
                    break;
            }
        }
    }
    catch (Exception e)
    {
        Console.WriteLine($"Error: {e.Message}");
    }

    Console.Write(Environment.NewLine);
}