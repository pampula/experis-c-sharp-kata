using you_shall_not_pass;

namespace you_shall_not_pass_tests;

public class UnitTest1
{
    [Theory]
    [InlineData("stonk", "Invalid")]
    [InlineData("pass word", "Invalid")]
    [InlineData("password", "Weak")]
    [InlineData("11081992", "Weak")]
    [InlineData("mySecurePass123", "Moderate")]
    [InlineData("!@!pass1", "Moderate")]
    [InlineData("@S3cur1ty", "Strong")]
    public void GetPasswordStrength_WithDifferentCriteriaMet_ShouldReturnCorrectStrength(string password, string expected)
    {
        // Arrange
        var sut = new PasswordChecker(); // sut = System Under Test
        // Act
        var result = sut.GetPasswordStrength(password);
        // Assert
        Assert.Equal(expected, result);
    }
}