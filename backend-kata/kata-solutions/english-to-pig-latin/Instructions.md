﻿KATA: English to Pig Latin Translator
Pig Latin has two very simple rules:
1.	If a word starts with a consonant move the first letter(s) of the word, till you reach a vowel, to the end of the word and add "ay" to the end.
have ➞ avehay
cram ➞ amcray
take ➞ aketay
cat ➞ atcay
shrimp ➞ impshray
trebuchet ➞ ebuchettray
2.	If a word starts with a vowel add "yay" to the end of the word.
ate ➞ ateyay
apple ➞ appleyay
oaken ➞ oakenyay
eagle ➞ eagleyay

Write two functions to make an English to pig Latin translator. The first function translateWord(word) takes a single word and returns that word translated into pig latin. The second function translateSentence(sentence) takes an English sentence and returns that sentence translated into pig Latin.
Examples
1.	translateWord("flag") ➞ "agflay"
2.	translateWord("Apple") ➞ "Appleyay"
3.	translateWord("button") ➞ "uttonbay"
4.	translateWord("") ➞ ""
5.	translateSentence("I like to eat honey waffles.") ➞ "Iyay ikelay otay eatyay oneyhay afflesway."
6.	translateSentence("Do you think it is going to rain today?") ➞ "Oday ouyay inkthay ityay isyay oinggay otay ainray odaytay?"
Notes
Regular expressions/LINQ will help you not mess up the punctuation in the sentence.
Vowels are the English vowels: a e i o u
If the original word or sentence starts with a capital letter, the translation should preserve its case (see examples #2, #5 and #6).
