﻿using english_to_pig_latin;

var translator = new PigLatinTranslator();

while (true)
{
    Console.WriteLine("Write something in English:");
    string? input = Console.ReadLine();
    if (!string.IsNullOrWhiteSpace(input))
    {
        Console.WriteLine(translator.TranslateSentence(input) + "\n");
    }
}