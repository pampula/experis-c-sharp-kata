﻿using System.Text;
using System.Text.RegularExpressions;

namespace english_to_pig_latin
{
    public class PigLatinTranslator
    {
        private static readonly char[] Vowels = { 'a', 'e', 'i', 'o', 'u', 'y' };

        private string TranslateWord(string word)
        {
            string result = "";
            string specialChars = "[^a-zA-Z]";
            string alphabet = "[a-zA-Z]";
            word = word.Trim();
            
            if (Vowels.Contains(Char.ToLower(word[0])))
            {
                result = word + "yay";
            }
            else
            {
                result =  String.Join("", word.TakeWhile(letter => !Vowels.Contains(letter)).ToArray());
                result = string.Concat(word.Substring(result.Length) + result + "ay").ToLower();
            }
            
            if (Char.IsUpper(word[0]))
            {
                result = Char.ToUpper(result[0]) + result.Substring(1).ToLower();
            }
            
            return Regex.Replace(result, specialChars, "") + Regex.Replace(result, alphabet, "");
        }

        public string TranslateSentence(string sentence)
        {
            sentence = sentence.Trim();

            if (String.IsNullOrEmpty(sentence))
            {
                return "";
            }
            
            var sb = new StringBuilder();
            string[] wordArray = sentence.Split(" ");
            for(int i = 0; i < wordArray.Length; ++i)
            {
                string translated = TranslateWord(wordArray[i]);
                sb.Append(i != 0 ? translated.ToLower() : translated);
                sb.Append(i < wordArray.Length - 1 ? " " : "");
            }
            
            return sb.ToString();
        }
    }
}