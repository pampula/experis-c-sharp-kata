using _005_Human_Readable_Time;

namespace human_readable_time_tests
{
    public class _005_Human_Readable_Time_Tests
    {
        [Theory]
        [InlineData(0, "00:00:00")]
        [InlineData(5, "00:00:05")]
        [InlineData(60, "00:01:00")]
        [InlineData(915, "00:15:15")]
        [InlineData(86399, "23:59:59")]
        [InlineData(359999, "99:59:59")]
        [InlineData(1000000, "99:59:59")]
        public void Generate_ShouldReturnCorrectTime_IfNumberIsProvided(int number, string expected)
        {
            // Arrange
            var sut = new TimeConverter(); // sut = System Under Test
            // Act
            var result = sut.GetReadableTime(number);
            // Assert
            Assert.Equal(expected, result);
        }
    }
}