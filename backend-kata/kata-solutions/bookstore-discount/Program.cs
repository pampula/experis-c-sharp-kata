﻿using bookstore_discount;

var calculator = new DiscountCalculator();

while (true)
{
    while (true)
    {
        bool inserting = true;
        List<int> purchasedBooks = new List<int>();
        Console.Clear();
        Console.WriteLine("Bookstore calculator.");
        while (inserting)
        {
            Console.WriteLine("\nInsert book index (0-3) or press C to calculate price:");
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.D0:
                    purchasedBooks.Add(0);
                    break;
                case ConsoleKey.D1:
                    purchasedBooks.Add(1);
                    break;
                case ConsoleKey.D2:
                    purchasedBooks.Add(2);
                    break;
                case ConsoleKey.D3:
                    purchasedBooks.Add(3);
                    break;
                case ConsoleKey.C:
                    inserting = false;
                    break;
            }
        }

        Console.WriteLine($"Price: {calculator.Price(purchasedBooks.ToArray())}\nPress any key to continue...");
        Console.ReadKey();
    }
}