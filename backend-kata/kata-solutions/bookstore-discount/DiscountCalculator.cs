﻿namespace bookstore_discount
{
    public class DiscountCalculator
    {
        private static int _singleBookPrice = 8;
        public decimal Price(int[] purchasedBooks)
        {
            decimal totalPrice = 0;
            int[] bookCounts = new int[4];

            foreach (var bookNumber in purchasedBooks)
            {
                if (bookNumber < 0 || bookNumber > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                bookCounts[bookNumber]++;
            }

            bool foundBooks = true;

            while (foundBooks)
            {
                var indexes = bookCounts.Select((v, i) => new { v, i })
                    .Where(x => x.v > 0)
                    .Select(x => x.i);

                if (indexes.Any())
                {
                    decimal discount = 1.00m;

                    discount = indexes.Count() switch
                    {
                        2 => 0.95m,
                        3 => 0.90m,
                        4 => 0.80m,
                        _ => 1.00m
                    };

                    totalPrice += indexes.Count() * discount * _singleBookPrice;
                    
                    foreach (var bookIndex in indexes)
                    {
                        bookCounts[bookIndex]--;
                    }
                }
                else
                {
                    foundBooks = false;
                }
            }
            
            return totalPrice;
        }
    }
}