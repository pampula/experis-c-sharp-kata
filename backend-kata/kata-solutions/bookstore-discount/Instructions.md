﻿KATA: Bookstore discount
The twilight series of books has been in a slump for a while and your local import bookstore is trying to do what it can to get the young tweens onto team Jacob or Edward. There are 4 books in the twilight series, the books store wants to encourage people to purchase multiple different ones – they are doing this by implementing a stackable discount. One book costs 8EUR. If, however, you buy two different books from the series, you get a 5% discount on those two books. If you buy 3 different books, you get a 10% discount. With 4 different books, you get a 20% discount.
Note: that if you buy, say, 4 books, of which 3 are different titles, you get a 10% discount on the 3 that form part of a set, but the fourth book still costs 8 EUR. This is part of the optional extra.
You are to write a testable solution to this. It should be a simple method that takes an array of integers (books 0-3) and outputs the cost. There are some test cases listed below that you can incorporate.
NOTE: I am using magic variables for readability 
Basic tests:
Assert.Equal(0, price([]))
Assert.Equal (8, price([1]))
Assert.Equal (8, price([2]))
Assert.Equal (8, price([3]))
Assert.Equal (8 * 3, price([1, 1, 1]))
Discount tests:
Assert.Equal (8 * 2 * 0.95, price([0, 1]))
Assert.Equal (8 * 3 * 0.9, price([0, 2, 3]))
Assert.Equal (8 * 4 * 0.8, price([0, 1, 2, 3]))
Optional extra – multiple discounts (will require some optimization about costs):
Assert.Equal (8 + (8 * 2 * 0.95), price([0, 0, 1]))
Assert.Equal (2 * (8 * 2 * 0.95), price([0, 0, 1, 1]))
Assert.Equal ((8 * 4 * 0.8) + (8 * 2 * 0.95), price([0, 0, 1, 2, 2, 3]))
