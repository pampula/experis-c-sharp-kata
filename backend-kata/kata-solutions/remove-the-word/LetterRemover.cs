﻿namespace remove_the_word
{
    public class LetterRemover
    {
        public void ShowPrompt()
        {
            while (true)
            {
                Console.WriteLine("Insert letters.");
                string? letters = Console.ReadLine();
                if (letters != null && letters.Length > 0)
                {
                    Console.WriteLine("Insert letters you want to remove.");
                    string? removed = Console.ReadLine();
                    if (removed != null && removed.Length > 0)
                    {
                        Console.WriteLine($"Result: {new string(RemoveLetters(letters, removed))}\n");
                    }
                }
            }
        }
        
        /// <summary>
        /// Filters chars from input string. Each letter in the second parameter is only removed once.
        /// </summary>
        /// <param name="letters">Original string.</param>
        /// <param name="removedLetters">Letters to be removed.</param>
        /// <returns>Filtered string.</returns>
        public string RemoveLetters(string letters, string removedLetters)
        {
            var removed = new List<char>(removedLetters);
            var remaining = new List<char>(letters);
            
            for (int i = 0; i < remaining.Count; ++i)
            {
                for (int j = removed.Count - 1; j >= 0; --j)
                {
                    if (remaining[i] == removed[j])
                    {
                        remaining.Remove(remaining[i]);
                        removed.Remove(removed[j]);
                        i--;
                        break;
                    }
                }
            }

            return new string(remaining.ToArray());
        }
    }
}