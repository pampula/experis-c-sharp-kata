﻿// Finds a) maximum difference and  b) pairs of values that add up to that difference

var charsToTrim = new[] { '{', '}'};

while (true)
{
    Console.WriteLine("Input numbers in array format:");
    var input = Console.ReadLine();
    try
    {
        var maxDiff = 0;
        input = input.Trim(charsToTrim);
        var numbers = input.Split(',');
        for (int i = 0; i < numbers.Length - 1; ++i)
        {
            for (int j = i + 1; j < numbers.Length; ++j)
            {
                var diff = int.Parse(numbers[j]) - int.Parse(numbers[i]);

                if (diff > maxDiff)
                {
                    maxDiff = diff;
                }
            }
        }
        
        Console.WriteLine(Environment.NewLine);
        Console.WriteLine($"The maximum difference between two array elements is:\n{maxDiff}");
        Console.WriteLine(Environment.NewLine);
        GetMatchingPairs(numbers, maxDiff);
    }
    catch(Exception e)
    {
        Console.WriteLine($"Input not valid: {e.Message}");
    }
}

void GetMatchingPairs(string[] numbers, int diff)
{
    List<Tuple<int, int, int, int>> sumValues = new List<Tuple<int, int, int, int>>();
    // Item1: index of first value
    // Item2: first value
    // Item3: index of second value
    // Item4: second value
    
    for (int i = 0; i < numbers.Length - 1; ++i)
    {
        for (int j = i + 1; j < numbers.Length; ++j)
        {
            var sum = int.Parse(numbers[j]) + int.Parse(numbers[i]);

            if (diff == sum)
            {
                sumValues.Add(new Tuple<int, int, int, int>(i, int.Parse(numbers[i]), j, int.Parse(numbers[j])));
            }
        }
    }

    Console.WriteLine($"There are {sumValues.Count} pair(s) that add up to the maximum.");
    foreach (Tuple<int,int,int,int> t in sumValues)
    {
        Console.WriteLine($"{t.Item2}({t.Item1}) + {t.Item4}({t.Item3}) = {t.Item2 + t.Item4}");
    }
    Console.WriteLine(Environment.NewLine);
}