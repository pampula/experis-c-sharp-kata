using english_to_pig_latin;

namespace english_to_pig_latin_tests;

public class UnitTest1
{
    [Theory]
    [InlineData("have", "avehay")]
    [InlineData("cram", "amcray")]
    [InlineData("take", "aketay")]
    [InlineData("cat", "atcay")]
    [InlineData("shrimp", "impshray")]
    [InlineData("trebuchet", "ebuchettray")]
    [InlineData("ate", "ateyay")]
    [InlineData("apple", "appleyay")]
    [InlineData("oaken", "oakenyay")]
    [InlineData("eagle", "eagleyay")]
    [InlineData("  eagle  ", "eagleyay")]
    [InlineData("flag", "agflay")]
    [InlineData("Apple", "Appleyay")]
    [InlineData("button", "uttonbay")]
    [InlineData("I like to eat honey waffles.", "Iyay ikelay otay eatyay oneyhay afflesway.")]
    [InlineData("Do you think it is going to rain today?", "Oday youyay inkthay ityay isyay oinggay otay ainray odaytay?")]
    public void TranslateSentences(string input, string expected)
    {
        // Arrange
        var sut = new PigLatinTranslator();
        // Act
        string result = sut.TranslateSentence(input);
        // Assert
        Assert.Equal(expected, result);
    }
}