using valid_credit_card;

namespace valid_credit_card_tests;

public class UnitTest1
{
    [Theory]
    [InlineData(79927398714, false)]
    [InlineData(79927398713, false)]
    [InlineData(709092739800713, true)]
    [InlineData(1234567890123456, false)]
    [InlineData(12345678901237, true)]
    [InlineData(5496683867445267, true)]
    [InlineData(4508793361140566, false)]
    [InlineData(376785877526048, true)]
    [InlineData(36717601781975, false)]
    [InlineData(null, false)]

    public void Test1(ulong cardNumber, bool expected)
    {
        // Arrange
        var sut = new Challenge();
        // Act
        var result = sut.ValidateCard(cardNumber);
        // Assert 
        Assert.Equal(result, expected);
    }
}