﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace valid_rgb
{
    public class ColorUtil
    {
        private const int minValueRgb = 0; // 0 0 0, Black
        private const int minValueRgbSingle = 0;
        private const int maxValueRgbSingle = 255;
        private const int maxValueRgbTotal = 765; // 255 255 255, White
        private const int minValueRgba = 0; // 0 0 0 0 transparent black
        private const int minValueRgbaSingle = 0;
        private const int maxValueRgbaSingle = 255;
        private const int maxValueRgbaTotal = 766; // 255 255 255, opaque white

        private const int minValueAlpha = 0;
        private const int maxValueAlpha = 1;
        
        private static Regex regexRgb = new Regex(@"rgb(?!a)\(", RegexOptions.IgnoreCase);
        // private static Regex regexRgbValues = new Regex (@"(\(\w+\))", RegexOptions.IgnoreCase);

        private static Regex regexRgba = new Regex(@"rgba", RegexOptions.IgnoreCase);
        // private static Regex regexRgbaValues = new Regex("rgba", RegexOptions.IgnoreCase);

        public string ValidRgbColour(string input)
        {
            string message = "";
            try
            {
                if (new Regex(@"rgb(\s.*)").IsMatch(input) || new Regex(@"rgba(\s.*)").IsMatch(input))
                {
                    message = "INVALID: whitespace before parenthesis";
                }
                else if (regexRgb.IsMatch(input))
                {
                    string pattern = @".*?(\(.*\))?";
                    input = Regex.Replace(Regex.Replace(input,@".*rgb(?!a)\(", ""), @"\).*", "");
                    var inputArray = input.Split(',');
                    if (inputArray.Length != 3)
                    {
                        message = $"INVALID: rgb with {inputArray.Length} numbers";
                    }
                    else
                    {
                        var sum = 0;
                        bool hasWhiteSpace = false;
                        bool isEmpty = false;
                        bool negativeNumbers = false;
                        bool numbersAbove255 = false;
                        bool isPercentage = false;
                        bool hasPercentage = false;
                        
                        for (int i = 0; i < inputArray.Length; ++i)
                        {
                            var value = inputArray[i];
                            var trimmed = inputArray[i].Trim();
                            isEmpty = trimmed.Length == 0;

                            if (isEmpty)
                            {
                                break;
                            }
                            
                            if (!hasWhiteSpace)
                            {
                                hasWhiteSpace = value.Length != trimmed.Length;
                            }
                            
                            isPercentage = trimmed[^1] == '%';
                            if (isPercentage)
                            {
                                hasPercentage = true;
                                trimmed = trimmed.Substring(0, trimmed.Length - 1);
                            }

                            if (int.TryParse(trimmed, out int parsedValue))
                            {
                                if (isPercentage)
                                {
                                    parsedValue = (int)((float)parsedValue / 100.0 * 255);
                                }
                                
                                if (parsedValue < minValueRgbSingle)
                                {
                                    negativeNumbers = true;
                                    break;
                                }
                                else if (parsedValue > maxValueRgbSingle)
                                {
                                    numbersAbove255 = true;
                                    break;
                                }
                                sum += parsedValue;
                            }
                            else
                            {
                                throw new ArgumentException();
                                Console.ReadLine();
                            }
                        }

                        if (numbersAbove255)
                        {
                            message = $"INVALID: numbers above {(isPercentage ? "100%" : "255")}";
                        }
                        else if (negativeNumbers)
                        {
                            message = $"INVALID: numbers below {(isPercentage ? "0%" : "0")}";
                        }
                        else if (isEmpty)
                        {
                            message = "INVALID: missing number";
                        }
                        else if (hasPercentage)
                        {
                            message = "numbers can be percentages";
                        }
                        else if (hasWhiteSpace)
                        {
                            message = "whitespace is allowed around numbers (even tabs)";
                        }
                        else if (sum == minValueRgb)
                        {
                            message = "rgb lowest valid numbers";
                        }
                        else if (sum == maxValueRgbTotal)
                        {
                            message = "rgb highest valid numbers";
                        }
                    }
                }
                else if (regexRgba.IsMatch(input))
                {
                    string pattern = @".*?(\(.*\))?";
                    input = Regex.Replace(Regex.Replace(input,@".*rgba\(", ""), @"\).*", "");
                    var inputArray = input.Split(',');

                    if (inputArray.Length != 4)
                    {
                        message = $"INVALID: rgba with {inputArray.Length} numbers";
                    }
                    else
                    {
                        float sum = 0;
                        bool hasWhiteSpace = false;
                        bool isEmpty = false;
                        bool negativeNumbers = false;
                        bool numbersAbove255 = false;
                        bool negativealpha = false;
                        bool alphaAbove1 = false;
                        bool alphaHasDecimals = false;
                        bool noLeadingZero = false;
                        bool hasPercentage = false;
                        bool isPercentage = false;
                        
                        for (int i = 0; i < inputArray.Length; ++i)
                        {
                            var value = inputArray[i];
                            var trimmed = inputArray[i].Trim();
                            isEmpty = trimmed.Length == 0;

                            if (isEmpty)
                            {
                                break;
                            }
                            
                            if (!hasWhiteSpace)
                            {
                                hasWhiteSpace = value.Length != trimmed.Length;
                            }

                            var culture = CultureInfo.GetCultureInfo("en-EN");
                            isPercentage = trimmed[^1] == '%';
                            if (isPercentage)
                            {
                                hasPercentage = true;
                                trimmed = trimmed.Substring(0, trimmed.Length - 1);
                            }
                            
                            if (float.TryParse(trimmed, NumberStyles.Any, culture, out float parsedValue))
                            {
                                if (isPercentage)
                                {
                                    parsedValue = parsedValue / 100 * (i < 3 ? 255 : 1);
                                }
                                if (i < 3)
                                {
                                    if (parsedValue < minValueRgbaSingle)
                                    {
                                        negativeNumbers = true;
                                        break;
                                    }
                                    else if (parsedValue > maxValueRgbaSingle)
                                    {
                                        numbersAbove255 = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (parsedValue < minValueAlpha)
                                    {
                                        negativealpha = true;
                                        break;
                                    }
                                    else if (parsedValue > maxValueAlpha)
                                    {
                                        alphaAbove1 = true;
                                        break;
                                    }
                                    else if (trimmed.ToString(culture)[0] == '.')
                                    {
                                        noLeadingZero = true;
                                    }
                                    else if (parsedValue.ToString(culture).Length > 3) // Two decimals
                                    {
                                        alphaHasDecimals = true;
                                    }
                                }

                                sum += parsedValue;
                            }
                            else
                            {
                                Console.WriteLine($"Could not parse {trimmed}");
                                throw new ArgumentException();
                            }
                        }

                        if (numbersAbove255)
                        {
                            message = $"INVALID: numbers above {(isPercentage ? "100%" : "255")}";
                        }
                        else if (negativeNumbers)
                        {
                            message = $"INVALID: numbers below {(isPercentage ? "0%" : "0")}";
                        }
                        else if (negativealpha)
                        {
                            message = "INVALID: alpha below 0";
                        }
                        else if (alphaAbove1)
                        {
                            message = "INVALID: alpha above 1";
                        }
                        else if (isEmpty)
                        {
                            message = "INVALID: missing number";
                        }
                        else if (hasPercentage)
                        {
                            message = "numbers can be percentages";
                        }
                        else if (noLeadingZero)
                        {
                            message = "in alpha the number before the dot is optional";
                        }
                        else if (alphaHasDecimals)
                        {
                            message = "alpha can have many decimals";
                        }
                        else if (hasWhiteSpace)
                        {
                            message = "whitespace is allowed around numbers (even tabs)";
                        }
                        else if (sum == minValueRgb)
                        {
                            message = "rgba lowest valid numbers";
                        }
                        else if (sum == maxValueRgbaTotal)
                        {
                            message = "rgba highest valid numbers";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            // Console.WriteLine($"{message}\n");
            return message;
        }
        
    }
}