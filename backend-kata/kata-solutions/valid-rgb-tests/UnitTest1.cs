using valid_rgb;

namespace valid_rgb_tests;

public class UnitTest1
{
    [Theory]
    [InlineData("rgb(0,0,0)", "rgb lowest valid numbers")]
    [InlineData("rgb(255,255,255", "rgb highest valid numbers")]
    [InlineData("rgba(0,0,0,0)", "rgba lowest valid numbers")]
    [InlineData("rgba(255,255,255,1)", "rgba highest valid numbers")]
    [InlineData("rgba(0,0,0,0.123456789)", "alpha can have many decimals")]
    [InlineData("rgba(0,0,0,.8)", "in alpha the number before the dot is optional")]
    [InlineData("rgba( 0 , 127 , 255 , 0.1)", "whitespace is allowed around numbers (even tabs)")]
    [InlineData("rgb(0%,50%,100%)", "numbers can be percentages")]
    [InlineData("rgb(0,,0)", "INVALID: missing number")]
    [InlineData("rgb (0,0,0)", "INVALID: whitespace before parenthesis")]
    [InlineData("rgb(0,0,0,0)", "INVALID: rgb with 4 numbers")]
    [InlineData("rgba(0,0,0)", "INVALID: rgba with 3 numbers")]
    [InlineData("rgb(-1,0,0)", "INVALID: numbers below 0")]
    [InlineData("rgb(255,256,255)", "INVALID: numbers above 255")]
    [InlineData("rgb(100%,100%,101%)", "INVALID: numbers above 100%")]
    [InlineData("rgba(0,0,0,-1)", "INVALID: alpha below 0")]
    [InlineData("rgba(0,0,0,1.1)", "INVALID: alpha above 1")]
    public void Test1(string input, string expected)
    {
        // Arrange
        var sut = new ColorUtil();
        // Act
        string result = sut.ValidRgbColour(input);
        // Assert
        Assert.Equal(result, expected);
    }
}