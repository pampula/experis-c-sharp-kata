﻿using System.Diagnostics;
using System.Numerics;
using System.Text;

var sb = new StringBuilder();
var lastTwo = new BigInteger[2] {-1, -1};
var waitingForInput = true;
ClearInput();

while (true)
{
    waitingForInput = true;
    
    while (waitingForInput)
    {
        if (Console.ReadKey(true).Key == ConsoleKey.F)
        {
            try
            {
                ClearInput();
                InsertNextNumber();
                Console.WriteLine(sb.ToString());
            }
            catch (Exception e)
            {                
                ClearInput();
                Console.WriteLine($"Exception: {e.Message}");
            }

            waitingForInput = false;
        }
        else if (Console.ReadKey(true).Key == ConsoleKey.Q)
        {
            Console.Clear();
            Console.WriteLine("Quitting...");
            Environment.Exit(0);
        }
        else
        {
            var input = Console.ReadLine();
            try
            {
                ClearInput();
                var stopWatch = Stopwatch.StartNew();
                Console.WriteLine($"Value at index {input} is {GetNumberAtIndex(int.Parse(input)).ToString()}.");
                stopWatch.Stop();
                if (stopWatch.ElapsedMilliseconds > 50)
                {
                    Console.WriteLine($"It took {stopWatch.ElapsedMilliseconds}ms to calculate the value.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }
            waitingForInput = false;
        }
    }
}

void InsertNextNumber()
{
    if(lastTwo[0] == BigInteger.MinusOne && lastTwo[1] == BigInteger.MinusOne)
    {
        lastTwo[0] = 0;
        sb.Append('0');
    }
    else if(lastTwo[0] == BigInteger.Zero && lastTwo[1] == BigInteger.MinusOne)
    {
        lastTwo[1] = 1;
        sb.Append(", 1");
    }
    else
    {
        BigInteger sum = lastTwo[0] + lastTwo[1];
        lastTwo[0] = lastTwo[1];
        lastTwo[1] = sum;
        sb.Append(string.Concat(", ", sum.ToString()));
    }
}

BigInteger GetNumberAtIndex(int index)
{
    if (index < 0)
    {
        return -1;
    }
    
    switch (index)
    {
        case 0:
            return 0;
        case 1:
            return 1;
        default:
            BigInteger indexValue = 0;
            BigInteger lastTwoSum = 0;
            BigInteger[] tempLastTwo = new BigInteger[2]{0,1};
            for (int i = 1; i < index - 1; ++i)
            {
                lastTwoSum = tempLastTwo[0] + tempLastTwo[1];
                tempLastTwo[0] = tempLastTwo[1];
                tempLastTwo[1] = lastTwoSum;
            }

            indexValue = tempLastTwo[0] + tempLastTwo[1];
            return indexValue;
    }
}

static void ClearInput()
{
    Console.Clear();
    Console.WriteLine("Press 'F' to generate next value in Fibonacci Sequence.\nType numerical value to get the Fibonacci number of that index.\nType 'Q' to quit program.");
}