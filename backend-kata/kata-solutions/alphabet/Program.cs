﻿var alphabet = new List<char>(0);
Console.WriteLine("Input lower and uppercase alphabet by pressing 'L' and 'C'. Type 'E' to exit app.");

while (true)
{
    switch (Console.ReadKey(true).Key)
    {
        case ConsoleKey.C:
            var uppercase = char.ToUpper(GetNextAlphabet(alphabet.LastOrDefault((char)('A'-1))));
            alphabet.Add(uppercase);
            Console.Clear();
            Console.WriteLine(alphabet.ToArray());
            break;
        case ConsoleKey.L:
            var lowercase = char.ToLower(GetNextAlphabet(alphabet.LastOrDefault((char)('A'-1))));
            alphabet.Add(lowercase);
            Console.Clear();
            Console.WriteLine(alphabet.ToArray());
            break;
        case ConsoleKey.E:
            Console.WriteLine("Exit app");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Not supported. Please insert letters by pressing 'L' and 'C'. Type 'E' to exit app.");
            break;
    }
}

char GetNextAlphabet(char prevAlphabet)
{
    return char.ToUpper(prevAlphabet) == 'Z' ? 'A' : (char)(prevAlphabet + 1);
}