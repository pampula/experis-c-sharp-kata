KATA

Please write an alphabet in two configurations:
If you type letter “l” then small letters:

abcdef…

If you type “c” then capital letters:

ABCDEF…

If you type “e” then exit

For other letters please write to console:

“Not supported. Please type correct letter”

String.concat = “+”, string.format, StringBuilder