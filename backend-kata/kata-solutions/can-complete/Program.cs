﻿using can_complete;

Console.WriteLine("String checker!");
string? letters = "";
string? finalWord = "";
var stringChecker = new StringChecker();

while (true)
{
    Console.WriteLine("Insert letters as a string");
    letters = Console.ReadLine();
    Console.WriteLine("Insert final word as a string");
    finalWord = Console.ReadLine();
    if (!string.IsNullOrEmpty(letters) && !string.IsNullOrEmpty(finalWord))
    {
        Console.WriteLine($"Can complete: {stringChecker.CanComplete(letters, finalWord)}\n");
    }
    else
    {
        Console.WriteLine("Input is incorrect.\n");
    }
}