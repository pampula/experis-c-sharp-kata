﻿namespace can_complete
{
    public class StringChecker
    {
        public bool CanComplete(string letters, string finalWord)
        {
            var currentIndex = 0;
            foreach (var singleChar in finalWord)
            {
                if (singleChar == letters[currentIndex])
                {
                    currentIndex++;
                }
            }

            return currentIndex == letters.Length;
        }
    }
}