﻿KATA: Can complete?
An input string can be completed if additional letters can be added and no letters need to be taken away to match the word. Furthermore, the order of the letters in the input string must be the same as the order of letters in the final word.
Essentially, does the first parameter only contain letters that appear in the second parameter, and are those letters in the correct order. 
Hint: Finding the index could help here.
Create a function that, given an input string, determines if the word can be completed.
Examples:
CanComplete("butl", "beautiful") ➞ true
// We can add "ea" between "b" and "u", and "ifu" between "t" and "l".
CanComplete("butlz", "beautiful") ➞ false
// "z" does not exist in the word beautiful.
CanComplete("tulb", "beautiful") ➞ false
// Although "t", "u", "l" and "b" all exist in "beautiful", they are incorrectly ordered.
CanComplete("bbutl", "beautiful") ➞ false
// Too many "b"s, beautiful has only 1.
CanComplete("bbb", "bbb") ➞ true

Notes
Both string input and word will be lowercased.
You do not need to print the explanation like I have here to show the logic.