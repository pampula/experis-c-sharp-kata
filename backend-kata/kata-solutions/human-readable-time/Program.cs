﻿using _005_Human_Readable_Time;

var timeConverter = new TimeConverter();

while (true)
{
    Console.WriteLine("Insert time in seconds:");
    bool validInput = int.TryParse(Console.ReadLine(), out int inputValue);
    if (validInput && inputValue >= 0)
    {
        Console.WriteLine($"{timeConverter.GetReadableTime(inputValue)}\n");
    }
    else
    {
        Console.WriteLine("Input was not a proper positive numerical value.");
        Console.WriteLine(Environment.NewLine);
    }
}