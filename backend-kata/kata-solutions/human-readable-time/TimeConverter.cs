﻿namespace _005_Human_Readable_Time
{
    public class TimeConverter
    {
        public string GetReadableTime(int seconds)
        {
            seconds = Math.Clamp(seconds, 0, 359999); // Limit max value to 99:59:59
            TimeSpan ts = TimeSpan.FromSeconds(seconds);
            return $"{Math.Floor(ts.TotalHours):00}:{ts.Minutes:00}:{ts.Seconds:00}";
        }
    }
}