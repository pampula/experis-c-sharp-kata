using bookstore_discount;

namespace bookstore_discount_tests;

public class UnitTest1
{
    [Theory]
    [InlineData(0)]
    [InlineData(8, 1)]
    [InlineData(8, 2)]
    [InlineData(8, 3)]
    [InlineData(8 * 3, 1, 1, 1)]
    [InlineData(8 * 2 * 0.95, 0, 1)]
    [InlineData(8 * 3 * 0.90, 0, 2, 3)]
    [InlineData(8 * 4 * 0.8, 0, 1, 2, 3)]
    [InlineData(8 + (8 * 2 * 0.95), 0, 0, 1)]
    [InlineData(2 * (8 * 2 * 0.95), 0, 0, 1, 1)]
    [InlineData((8 * 4 * 0.8) + (8 * 2 * 0.95), 0, 0, 1, 2, 2, 3)]
    public void Test1(decimal expected, params int[] books)
    {
        // Arrange
        var sut = new DiscountCalculator();
        // Act
        var result = sut.Price(books);
        // Assert
        Assert.Equal(expected, result);
    }
}