using freddy_fridays;

namespace freddy_fridays_test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(2022, 1)]
        [InlineData(2026, 3)]
        [InlineData(2050, 1)]
        [InlineData(3000, 1)]
        public void HowManyFriday13th_InputIsValidYear_ReturnCount(int input, int expected)
        {
            // Arrange
            var sut = new FreddyFridays(); // sut = System Under Test
            // Act
            var result = sut.FindFreddyFridays(input);
            // Assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-5000)]
        [InlineData(3001)]
        public void HowManyFriday13th_InputIsInvalidYear_ThrowError(int input)
        {
            // Arrange
            var sut = new FreddyFridays(); // sut = System Under Test
            // Act
            Action act = () => sut.FindFreddyFridays(input);
            // Assert
            // Assert.Throws<Exception>(act);
            Exception exception = Assert.Throws<Exception>(act);
            Assert.Equal($"'{input}' is not a valid year.", exception.Message);
        }
        
        [Theory]
        [InlineData("asd")]
        [InlineData("2000.5")]
        public void HowManyFriday13th_InputIsNotProperNumericalValue_ThrowError(string input)
        {
            // Arrange
            var sut = new FreddyFridays(); // sut = System Under Test
            // Act
            Action act = () => sut.CheckValidInput(input, out var value);
            // Assert
            // Assert.Throws<Exception>(act);
            Exception exception = Assert.Throws<Exception>(act);
            Assert.Equal($"'{input}' is not a valid numerical input.", exception.Message);
        }
    }
}