﻿using System.Security.Cryptography.X509Certificates;

namespace valid_credit_card
{
    public class Challenge
    {
        public bool ValidateCard(ulong cardNumber)
        {
            try
            {
                List<int> digitArray = new List<int>();
                digitArray.AddRange(cardNumber.ToString().Select(d => (int)d - 48));
                int checkDigit = digitArray[^1];
                digitArray.RemoveAt(digitArray.Count - 1);
                digitArray.Reverse();
                IEnumerable<int> doubledOdds =
                    from pair in digitArray.Select((value, index) => new { value, index })
                    select (pair.index + 1) % 2 == 0
                        ? pair.value
                        : (pair.value * 2 >= 10
                            ? pair.value * 2 - 9
                            : pair.value * 2);
                int sum = doubledOdds.Sum();
                if (cardNumber.ToString().Length < 14 || cardNumber.ToString().Length > 20)
                {
                    return false;
                }
                else
                {
                    int result = 10 - sum % 10;
                    return result == checkDigit;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Input is not valid: {e.Message}");
                return false;
            }
        }   
    }
}