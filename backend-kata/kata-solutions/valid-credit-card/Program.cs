﻿using valid_credit_card;

var checker = new Challenge();

Console.WriteLine("Credit card number validator.");
while (true)
{
    Console.WriteLine("Insert credit card number:");
    try
    {
        Console.WriteLine($"Number is valid: {checker.ValidateCard(UInt64.Parse(Console.ReadLine()))}\n");
    }
    catch(Exception e)
    {
        Console.WriteLine($"Incorrect input: {e.Message}.\n");
    }
}