﻿using System.Diagnostics;
using System.Text;

while (true)
{
    Console.WriteLine("Insert FizzBuzz max value:");
    bool validInput = int.TryParse(Console.ReadLine(), out int inputValue);
    if (validInput && inputValue > 0)
    {
        Console.WriteLine($"Result: {ConvertFizzBuzz(inputValue)}");
        Console.WriteLine(Environment.NewLine);
    }
    else
    {
        Console.WriteLine("Input was not a proper positive numerical value.");
        Console.WriteLine(Environment.NewLine);
    }
}

string ConvertFizzBuzz(int numberCount)
{
    var sb = new StringBuilder();
    sb.Append('1');
    for (int i = 2; i <= numberCount; ++i)
    {
        sb.Append(
            (i % 3, i % 5) switch
            {
                (0, 0) => ", FizzBuzz",
                (_, 0) => ", Buzz",
                (0, _) => ", Fizz",
                (_, _) => $", {i.ToString()}"
            }
        );
    }
    
    return sb.ToString();
}