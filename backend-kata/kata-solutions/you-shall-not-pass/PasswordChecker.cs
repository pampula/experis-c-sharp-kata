﻿using System.Text.RegularExpressions;

namespace you_shall_not_pass;

public class PasswordChecker
{
    private readonly Regex _lowerCaseRegex = new Regex("[a-z]", RegexOptions.Compiled);
    private readonly Regex _upperCaseRegex = new Regex("[A-Z]", RegexOptions.Compiled);
    private readonly Regex _symbolRegex = new Regex("[^a-zA-Z0-9]", RegexOptions.Compiled);
    private readonly Regex _digitRegex = new Regex(@"[\d]", RegexOptions.Compiled);

    public void AskForPassword()
    {
        Console.WriteLine("Insert your password.");
        while (true)
        {
            string input = Console.ReadLine();
            if (input.Length > 0)
            {
                Console.WriteLine(GetPasswordStrength(input));
            }
        }
    }

    public string GetPasswordStrength(string input)
    {
        if (input.Contains(" ") || input.Length < 6)
        {
            return "Invalid";
        }

        int criteriaMet = 0;

        if (input.Length >= 8)
        {
            criteriaMet++;
        }

        criteriaMet += _lowerCaseRegex.Match(input).Success ? 1 : 0;
        criteriaMet += _upperCaseRegex.Match(input).Success ? 1 : 0;
        criteriaMet += _symbolRegex.Match(input).Success ? 1 : 0;
        criteriaMet += _digitRegex.Match(input).Success ? 1 : 0;

        switch (criteriaMet)
        {
            case 1:
            case 2:
                return "Weak";
            case 3:
            case 4:
                return "Moderate";
            default:
                return "Strong";
        }
    }
}