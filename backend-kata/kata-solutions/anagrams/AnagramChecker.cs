﻿using System.Text.RegularExpressions;

namespace anagrams
{
    public class AnagramChecker
    {
        public bool IsAnagram(string firstString, string secondString)
        {
            string pattern = "[^a-zA-Z]";
            firstString = Regex.Replace(firstString, pattern, "").ToLower();
            secondString = Regex.Replace(secondString, pattern, "").ToLower();
            firstString = string.Concat(firstString.OrderBy(c => c));
            secondString = string.Concat(secondString.OrderBy(c => c));
            return firstString == secondString;
        }
    }
}