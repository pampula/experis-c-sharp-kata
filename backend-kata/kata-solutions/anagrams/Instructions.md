KATA: Anagrams
Check to see if two provided strings are anagrams of each other.
One string is an anagram of another f it uses the same characters in the same
quantity. Only consider characters, not spaces or punctuation. Consider capital to
be the same as lower case.
Examples:
isAnagram(‘rail safety’, ‘fairy tales’) → True
isAnagram(‘RAIL! SAFETY!’, ‘fairy tales’) → True
isAnagram(‘Hi there, ‘Bye there’) → False