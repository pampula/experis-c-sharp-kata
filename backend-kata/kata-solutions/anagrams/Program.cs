﻿using anagrams;

AnagramChecker ac = new AnagramChecker();

Console.WriteLine("Anagram Checker\n");
while (true)
{
    Console.WriteLine("Insert first word");
    string firstWord = Console.ReadLine();
    Console.WriteLine("Insert second word");
    string secondWord = Console.ReadLine();
    Console.WriteLine($"{firstWord} and {secondWord} are anagrams: {ac.IsAnagram(firstWord, secondWord)}\n");
}
