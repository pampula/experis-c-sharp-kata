﻿using reducto_multiplicitum;

var reducer = new Reducer();
Console.WriteLine("Reducto Multiplicitum!\n");

while (true)
{
    Console.WriteLine("Insert numbers separated by commas:");
    string? numberInput = Console.ReadLine();
    Console.WriteLine($"Result: {reducer.SumDigitProd(numberInput)}\n");
}