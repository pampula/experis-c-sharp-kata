﻿namespace reducto_multiplicitum
{
    public class Reducer
    {
        public int? SumDigitProd(string? numbers)
        {
            int? result = null;
            try
            {
                if (!string.IsNullOrEmpty(numbers))
                {
                    numbers = numbers.Trim();
                    int[] numberArray = numbers.Split(',').Select(n => Convert.ToInt32(n.Trim())).ToArray();
                    var sum = numberArray.Sum();
                    result = Reducto(sum);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}\n");
            }

            return result;
        }

        int Reducto(int number)
        {
            if (number < 10 && number > -10) return number;

            var result = 1;
            foreach (var digit in number.ToString())
            {
                result *= int.Parse(digit.ToString());
            }

            return Reducto(result);
        }
    }
}