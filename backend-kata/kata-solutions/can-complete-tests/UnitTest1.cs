using can_complete;

namespace can_complete_tests;

public class UnitTest1
{
    [Theory]
    [InlineData("butl", "beautiful")]
    [InlineData("bbb", "bbb")]
    public void CanComplete_WhenLettersMatch_ReturnTrue(string letters, string finalWord)
    {
        // Arrange
        var sut = new StringChecker();
        // Act
        var result = sut.CanComplete(letters, finalWord);
        bool expected = true;
        // Assert
        Assert.Equal(expected, result);
    }
    
    [Theory]
    [InlineData("butlz", "beautiful")]
    [InlineData("bbbb", "bbb")]
    [InlineData("tulb", "beautiful")]
    [InlineData("bbutl", "beautiful")]
    public void CanComplete_WhenLettersDoNotMatch_ReturnFalse(string letters, string finalWord)
    {
        // Arrange
        var sut = new StringChecker();
        // Act
        var result = sut.CanComplete(letters, finalWord);
        bool expected = false;
        // Assert
        Assert.Equal(expected, result);
    }
}