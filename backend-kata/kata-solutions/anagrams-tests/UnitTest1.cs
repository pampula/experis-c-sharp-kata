using anagrams;

namespace anagrams_tests;

public class UnitTest1
{
    [Theory]
    [InlineData("rail safety", "fairy tales")]
    [InlineData("RAIL! SAFETY!", "fairy tales")]
    [InlineData("#¤)g#!¤lg¤)#(!#", "G00GL3")]
    [InlineData("RAIL! SAFETY!", "fairy tales")]
    public void IsAnagram_WhenSameCharacters_ShouldReturnTrue(string first, string second)
    {
        // Arrange
        var sut = new AnagramChecker();
        // Act
        var expected = true;
        var result = sut.IsAnagram(first, second);
        // Assert
        Assert.Equal(expected, result);
    }
    
    [Theory]
    [InlineData("Hi there", "Bye there")]
    [InlineData("a", "")]
    public void IsAnagram_WhenDifferentCharacters_ShouldReturnFalse(string first, string second)
    {
        // Arrange
        var sut = new AnagramChecker();
        // Act
        var expected = false;
        var result = sut.IsAnagram(first, second);
        // Assert
        Assert.Equal(expected, result);
    }
}